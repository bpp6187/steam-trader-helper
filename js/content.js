"use strict";

chrome.runtime.onMessage.addListener(
	function(message, callback) {

		if (message.action == "alert") {
			alert(message.text);
		}
	}
);

window.addEventListener('message', function(event) {

	if(event.data.method) {
		chrome.runtime.sendMessage({'method': event.data.method, 'data': event.data.data }, function(response) {

		});
	}
});